package com.example.sqldemo;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.widget.Button;
import android.widget.EditText;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StudentDB s= new StudentDB("stud");
        Button i = findViewById(R.id.ins);
        Button d = findViewById(R.id.del);
        Button u = findViewById(R.id.upd);
        Button vi = findViewById(R.id.vi);
        EditText nm = findViewById(R.id.nm);
        EditText reg = findViewById(R.id.rg);
        EditText mr = findViewById(R.id.mr);
        EditText res = findViewById(R.id.res);
        i.setOnClickListener(v -> s.insert(nm.getText().toString(),Integer.parseInt(reg.getText().toString()),Float.parseFloat(mr.getText().toString())));
        d.setOnClickListener(v-> s.delete(Integer.parseInt(reg.getText().toString())));

        vi.setOnClickListener(v->{
            Vector<String> x = s.view();
            for (String value : x) res.append(value + "\n");

        });
        u.setOnClickListener(v->{
            s.update(Integer.parseInt(reg.getText().toString()),Float.parseFloat(mr.getText().toString()));
        });

        if (SDK_INT >= Build.VERSION_CODES.R) {

            if (!Environment.isExternalStorageManager()) { //request for the permission
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        } else {
            //below android 11==s=====
            startActivity(new Intent(this, MainActivity.class));
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, 100);
        }

    }
}