package com.example.sqldemo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Vector;

public class StudentDB {
    SQLiteDatabase d;
    StudentDB(String name){
        try {
            d = SQLiteDatabase.openOrCreateDatabase("/sdcard/"+name+".db", null);
            d.execSQL("CREATE TABLE IF NOT EXISTS  student(reg INT(4),name VARCHAR,marks FLOAT(10));");
        }catch (Exception e) {

        }
    }
   void insert(String name, int reg,float marks){
        String query = String.format("INSERT INTO student VALUES(%d,'%s',%f)",reg,name,marks);
        d.execSQL(query);
   }
   String delete(int reg){
       String query = String.format("DELETE FROM student where reg=%d",reg);
       d.execSQL(query);
       return "Success";
   }
   String update(int reg, float marks){
       String query = String.format("UPDATE student set marks=%f WHERE reg=%d",marks,reg);
       d.execSQL(query);
        return "Success";
   }
    Vector<String> view(){
        Vector<String> ret = new Vector<>(100);
        String query = "SELECT * FROM student";
        Cursor c = d.rawQuery(query,null);
        ret.add("REG\tName\tMarks");
        while(c.moveToNext()){
            ret.add(String.format("%s\t%s\t%s", c.getInt(0),c.getString(1), c.getFloat(2)));

        }
        return ret;
    }
}
